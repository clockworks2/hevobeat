#!/usr/bin/python3

from time import time
from flask import Flask
import requests
import subprocess
import json
import argparse
from threading import Timer

app = Flask(__name__)
watchdog = None
watchdogTimeout = 120
requestInterval = 10

parser = argparse.ArgumentParser(description="HevoBeat")
parser.add_argument('--localPort', required=True, type=int)
parser.add_argument('--remotePort', required=True, type=int)
parser.add_argument('--remoteAddress', required=True)
parser.add_argument('--service', required=True)
parser.add_argument('--watchdogInterval', required=True, type=int)
parser.add_argument('--heartbeatInterval', required=True, type=int)


@app.route("/heartbeat")
def getHeartbeat():
    return json.dumps({"time": time()})


def mainServer(port):
    app.run(port=port)


def resetTunnels(service):
    global watchdog
    watchdog = None
    turnOff = subprocess.run('systemctl stop {}'.format(service).split(' '))
    if turnOff.returncode != 0:
        print("Failed to stop service")
    turnOn = subprocess.run('systemctl start {}'.format(service).split(' '))
    if turnOn.returncode != 0:
        print("Failed to start service")
    watchdog = Timer(watchdogTimeout, resetTunnels, args=[service])
    watchdog.start()


def checkHeartbeat(url, service):
    global watchdog
    Timer(requestInterval, checkHeartbeat, args=[url, service]).start()
    if not watchdog:
        return
    try:
        heartbeatPacket = requests.get(url, timeout=2)
        if heartbeatPacket.status_code == 200:
            watchdog.cancel()
            watchdog = Timer(watchdogTimeout, resetTunnels, args=[service])
            watchdog.start()
        else:
            print("Failed to get heartbeat: status code {}".format(heartbeatPacket.status_code))
    except requests.exceptions.Timeout:
        print("Failed to get heartbeat: timeout")


def mainClient(host, port, service):
    # reset watchdog set to 2 minutes
    # timer every 20 seconds. reset watchdog if ok.
    # if watchdog expires, reset it and restart the network stack.
    global watchdog
    watchdog = Timer(watchdogTimeout, resetTunnels, args=[service])
    watchdog.start()
    url = "https://{}:{}/heartbeat".format(host, port)
    Timer(requestInterval, checkHeartbeat, args=[url, service]).start()


if __name__ == '__main__':
    global watchdogTimeout
    global requestInterval
    args = parser.parse_args()
    if args.watchdogInterval > 0:
        watchdogTimeout = args.watchdogInterval
    if args.heartbeatInterval > 0:
        requestInterval = args.heartbeatInterval
    print("Starting HevoBeat client component")
    mainClient(args.remoteAddress, args.remotePort, args.service)
    print("Starting HevoBeat server component")
    mainServer(args.localPort)
